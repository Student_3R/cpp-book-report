#include <stdio.h>
#include <stdlib.h>

int main()
{

    int p = 10;
    int *ptr = &p;
    int **ptr2 = &ptr;
    int ***ptr3 = &ptr2;

    printf("取得p的記憶體位置: %p\n", &p);
    printf("取得p值: %d\n", p);

    printf("*ptr取得到的p值: %d\n", *ptr);
    printf("ptr指向的記憶體位址: %p\n", ptr);
    printf("取得ptr自身的記憶體位址: %p\n\n\n", &ptr);
 
    printf("**ptr2取得到的p值:%d\n", **ptr2);
    printf("*ptr2指向p記憶體位置:%p\n", *ptr2);
    printf("ptr2指向ptr1的記憶體位置:%p\n", ptr2);
    printf("取得ptr2記憶體位址:%p\n\n\n", &ptr2);

    printf("***ptr3取得到p的值:%d\n", ***ptr3);
    printf("**ptr3指向p記憶體位置:%p\n", **ptr3);
    printf("*ptr3指向ptr1的記憶體位置:%p\n", *ptr3);
    printf("ptr3指向ptr2的記憶體位置:%p\n", ptr3);
    printf("取得ptr3記憶體位址:%p\n", &ptr3);
    printf("&***ptr3記憶體位址:%p\n", &***ptr3);
    return 1;
}